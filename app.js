/**
 * Module dependencies.
 */

var express = require('express')
    , ac = require('atlassian-connect-express')
    , http = require('http')
    , os = require('os')
    , routes = require('./config/router');

// Static expiry middleware to helpe serve static resources efficiently
process.env.PWD = process.env.PWD || process.cwd(); // Fix expiry on Windows :(

// Bootstrap Express
var app = express();
// Bootstrap the `atlassian-connect-express` library
var addon = ac(app);
// You can set this in `config.js`
var port = addon.config.port();
var homeDir = __dirname;

//create socke.io server
var server = http.createServer(app);
var sio = require('socket.io').listen(server);

// express settings
require('./config/express')(app,addon,homeDir);

server.listen(port, function () {
    console.log('Add-on server running at http://' + os.hostname() + ':' + port);
    //console.log(addon.config.hosts());
});

//Use long polling since Heroku doesn't support Websockets
sio.configure(function () {
    sio.set("transports", ["xhr-polling"]);
    sio.set("polling duration", 10);
});


// Wire up your routes using the express and `atlassian-connect-express` objects
routes(app, addon, sio);