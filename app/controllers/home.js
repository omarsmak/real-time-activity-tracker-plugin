/**
 *Root URL
 */

var Name = require('../models/Name');
var io = null,
    addon = null;

exports.init = function (_addon, _io){
    io = _io;
    addon = _addon;
}

exports.root = function (req, res) {
    res.format({
        // If the request content-type is text-html, it will decide which to serve up
        'text/html': function () {
            res.redirect(addon.descriptor.documentationUrl() || '/atlassian-plugin.xml');
        },
        // This logic is here to make sure that the `atlassian-plugin.xml` is always
        // served up when requested by the host.
        'application/xml': function () {
            res.redirect('/atlassian-plugin.xml');
        }
    });
}


exports.test = function (req, res){
    var omar = new Name('omar ahmed');
    var obff = {};

    console.log(omar.name);
}

exports.hello = function (req, res){
    res.render("hello-world");
}