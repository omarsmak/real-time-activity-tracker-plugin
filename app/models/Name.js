

function Name (name){
    this.name = name;
}

Name.prototype.say = function(){
    console.log(this.name);
}

module.exports = Name;