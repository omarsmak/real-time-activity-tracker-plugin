/**
 * Module dependencies.
 */
var express = require('express')
    , hbs = require('express-hbs')
    , expiry = require('static-expiry')
    , path = require('path');


module.exports = function (app, addon, homeDir) {
    var devEnv = app.get('env') == 'development';
    var staticDir = path.join(homeDir, 'public');
    var viewsDir = homeDir + '/app/views';
    //Set the port
    app.set('port', addon.config.port());
    //Set the view engine
    app.engine('hbs', hbs.express3({partialsDir: viewsDir}));
    app.set('view engine', 'hbs');
    app.set('views', viewsDir);

    // Declare any Express [middleware](http://expressjs.com/api.html#middleware) you'd like to use here
    app.use(express.favicon());
// Log requests, using an appropriate formatter by env
    app.use(express.logger(devEnv ? 'dev' : 'default'));
// Include stock request parsers
    app.use(express.bodyParser());
    app.use(express.cookieParser());
// Gzip responses when appropriate
    app.use(express.compress());
// atlassian-connect-express requires sessions; cookie sessions are useful for easy multi-dyno support on Heroku
    app.use(express.cookieSession({
        // Arbitrary key for the session cookie
        key: 'session',
        // Automatically generated secret based on your private key
        secret: addon.config.secret()
    }));
// You need to instantiate the `atlassian-connect-express` middleware in order to get its goodness for free
    app.use(addon.middleware());
// Enable static resource fingerprinting for far future expires caching in production
    app.use(expiry(app, {dir: staticDir, debug: devEnv}));
// Add an hbs helper to fingerprint static resource urls
    hbs.registerHelper('furl', function (url) {
        return app.locals.furl(url);
    });
// Mount the add-on's routes
    app.use(app.router);
// Mount the static resource dir
    app.use(express.static(staticDir));

    if (devEnv) app.use(express.errorHandler());

    //In case the routes are wrong
    app.use(function (req, res, next) {
        res.redirect('/');
    });

// error-handling middleware, take the same form
// as regular middleware, however they require an
// arity of 4, aka the signature (err, req, res, next).
// when connect has an error, it will invoke ONLY error-handling
// middleware.

// If we were to next() here any remaining non-error-handling
// middleware would then be executed, or if we next(err) to
// continue passing the error, only error-handling middleware
// would remain being executed, however here
// we simply respond with an error page.


    app.use(function (err, req, res, next) {
        res.redirect('/');
    });
}