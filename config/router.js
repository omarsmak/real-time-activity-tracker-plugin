
var home = require('../app/controllers/home');

module.exports = function (app, addon, io) {
    //Pass the variables to the router
    home.init(addon, io);

    app.get('/', home.root);
    app.get('/test', home.test);
    app.get('/hello-world', home.hello);
}